import book.theautomatedtester.co.uk.pages.HomePage;
import book.theautomatedtester.co.uk.pages.chapter1.Chapter1Page;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class MainTest {
	protected HomePage homePage         = new HomePage();
	protected Chapter1Page chapter1Page = new Chapter1Page();

	@BeforeMethod
	public void setUp(){
		open("http://book.theautomatedtester.co.uk");
	}

	@Test
	public void testLoginOnStage(){
		homePage.clickOnChapter1();
		chapter1Page.checkAssertFlag();
		chapter1Page.clickLinkHomePage();
		homePage.checkHomePageLoaded();
	}
}
