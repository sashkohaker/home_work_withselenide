package book.theautomatedtester.co.uk.pages.chapter1;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import org.testng.Assert;

import static com.codeborne.selenide.Selenide.$;

public class Chapter1Page {
	private SelenideElement assertFlag   = $(By.id("divontheleft"));
	private SelenideElement linkHomePage = $(By.xpath("//a[@href='/']"));

	public void checkAssertFlag(){
		assertFlag.isDisplayed();
		Assert.assertEquals(assertFlag.getText(), "Assert that this text is on the page");
	}

	public void clickLinkHomePage(){
		linkHomePage.click();
	}
}
