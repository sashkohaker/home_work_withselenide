package book.theautomatedtester.co.uk.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class HomePage {
	private SelenideElement linkChapter1 = $(By.xpath("//a[@*='/chapter1']"));

	public void clickOnChapter1(){
		linkChapter1.click();
	}

	public void checkHomePageLoaded(){
		linkChapter1.isDisplayed();
	}
}
